<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\IncomeClass;
use Faker\Generator as Faker;

$factory->define(IncomeClass::class, function (Faker $faker) {
    \App\IncomeClass::truncate();

    \App\IncomeClass::insert([
        [
            'id' => 1,
            'name' => 'শ্রেণী ১'
        ],
        [
            'id' => 2,
            'name' => 'শ্রেণী ২'
        ],
        [
            'id' => 3,
            'name' => 'শ্রেণী ৩'
        ],
        [
            'id' => 4,
            'name' => 'শ্রেণী ৪'
        ],
        [
            'id' => 5,
            'name' => 'শ্রেণী ৫'
        ],

    ]);
    return [
        'id' => 6,
        'name' => 'শ্রেণী ৬'
    ];
});
