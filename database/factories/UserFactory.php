<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    \App\User::truncate();
    return [
        'id' => 1,
        'name' => "Tanvir Sarker",
        'username' => 'dev',
        'user_type_id' => 1,
        'contact_number' => 'dev',
        'address' => $faker->address,
        'company_id' => 1,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt(1), // password
        'remember_token' => Str::random(60),
    ];
});
