<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ExpenseSector;
use Faker\Generator as Faker;

$factory->define(ExpenseSector::class, function (Faker $faker) {
    \App\ExpenseSector::truncate();
    \App\ExpenseSector::insert([
        [
            'id' => 1,
            'name' => 'মিল'
        ],
        [
            'id' => 2,
            'name' => 'লোড'
        ],
        [
            'id' => 3,
            'name' => 'আনলোড'
        ],
        [
            'id' => 4,
            'name' => 'ফায়ারিং'
        ],
        [
            'id' => 5,
            'name' => 'মাটি মিক্সিং'
        ],
        [
            'id' => 6,
            'name' => 'কয়লা'
        ],
        [
            'id' => 7,
            'name' => 'মাটি'
        ],
        [
            'id' => 8,
            'name' => 'বালু'
        ],
        [
            'id' => 9,
            'name' => 'তেল'
        ],
        [
            'id' => 10,
            'name' => 'মেশিন পার্টস'
        ],
        [
            'id' => 11,
            'name' => 'বেতন'
        ],
        [
            'id' => 12,
            'name' => 'ভ্যাট'
        ],
        [
            'id' => 13,
            'name' => 'লেবার'
        ],
        [
            'id' => 14,
            'name' => 'ভূমি'
        ],
        [
            'id' => 15,
            'name' => 'বিবিধ'
        ]
    ]);

    return [
        'id' => 16,
        'name' => 'খোরাকি'
    ];
});
