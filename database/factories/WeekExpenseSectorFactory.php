<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\WeekExpenseSector;
use Faker\Generator as Faker;

$factory->define(WeekExpenseSector::class, function (Faker $faker) {

    \App\WeekExpenseSector::truncate();
        \App\WeekExpenseSector::insert([
            [
                'week_expense_sector_name_id' => 1,
                'week_expense_sub_sector_name_id' => 1,
                'week_expense_type_name_id' => 1,
                'name' => 'test1',
                'date' => '2019-12-14',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 1,
                'week_expense_sub_sector_name_id' => 2,
                'week_expense_type_name_id' => 1,
                'name' => 'test1',
                'date' => '2019-12-14',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 2,
                'week_expense_sub_sector_name_id' => 1,
                'week_expense_type_name_id' => 1,
                'name' => 'test1',
                'date' => '2019-12-14',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 3,
                'week_expense_sub_sector_name_id' => 0,
                'week_expense_type_name_id' => 1,
                'name' => 'test1',
                'date' => '2019-12-15',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 3,
                'week_expense_sub_sector_name_id' => null,
                'week_expense_type_name_id' => 2,
                'name' => 'test1',
                'date' => '2019-12-16',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 3,
                'week_expense_sub_sector_name_id' => null,
                'week_expense_type_name_id' => 3,
                'name' => 'test1',
                'date' => '2019-12-22',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 3,
                'week_expense_sub_sector_name_id' => null,
                'week_expense_type_name_id' => 4,
                'name' => 'test1',
                'date' => '2019-12-23',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 1,
                'week_expense_sub_sector_name_id' => 1,
                'week_expense_type_name_id' => 2,
                'name' => 'test1',
                'date' => '2019-12-28',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 1,
                'week_expense_sub_sector_name_id' => 1,
                'week_expense_type_name_id' => 3,
                'name' => 'test1',
                'date' => '2019-12-16',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 1,
                'week_expense_sub_sector_name_id' => 2,
                'week_expense_type_name_id' => 3,
                'name' => 'test1',
                'date' => '2019-12-17',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 2,
                'week_expense_sub_sector_name_id' => 2,
                'week_expense_type_name_id' => 2,
                'name' => 'test1',
                'date' => '2019-12-17',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 2,
                'week_expense_sub_sector_name_id' => 2,
                'week_expense_type_name_id' => 3,
                'name' => 'test1',
                'date' => '2019-12-17',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 2,
                'week_expense_sub_sector_name_id' => 2,
                'week_expense_type_name_id' => 4,
                'name' => 'test1',
                'date' => '2019-12-17',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 3,
                'week_expense_sub_sector_name_id' => null,
                'week_expense_type_name_id' => 5,
                'name' => 'test1',
                'date' => '2019-12-17',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 4,
                'week_expense_sub_sector_name_id' => null,
                'week_expense_type_name_id' => 1,
                'name' => 'test1',
                'date' => '2019-12-17',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 4,
                'week_expense_sub_sector_name_id' => null,
                'week_expense_type_name_id' => 2,
                'name' => 'test1',
                'date' => '2019-12-19',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 4,
                'week_expense_sub_sector_name_id' => null,
                'week_expense_type_name_id' => 3,
                'name' => 'test1',
                'date' => '2019-12-19',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 4,
                'week_expense_sub_sector_name_id' => null,
                'week_expense_type_name_id' => 4,
                'name' => 'test1',
                'date' => '2019-12-19',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 5,
                'week_expense_sub_sector_name_id' => null,
                'week_expense_type_name_id' => 1,
                'name' => 'test1',
                'date' => '2019-12-19',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 5,
                'week_expense_sub_sector_name_id' => null,
                'week_expense_type_name_id' => 2,
                'name' => 'test1',
                'date' => '2019-12-19',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 5,
                'week_expense_sub_sector_name_id' => null,
                'week_expense_type_name_id' => 3,
                'name' => 'test1',
                'date' => '2019-12-18',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 1,
                'week_expense_sub_sector_name_id' => 3,
                'week_expense_type_name_id' => 1,
                'name' => 'test1',
                'date' => '2019-12-18',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 1,
                'week_expense_sub_sector_name_id' => 3,
                'week_expense_type_name_id' => 2,
                'name' => 'test1',
                'date' => '2019-12-18',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 1,
                'week_expense_sub_sector_name_id' => 3,
                'week_expense_type_name_id' => 3,
                'name' => 'test1',
                'date' => '2019-12-15',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ],
            [
                'week_expense_sector_name_id' => 1,
                'week_expense_sub_sector_name_id' => 3,
                'week_expense_type_name_id' => 4,
                'name' => 'test1',
                'date' => '2019-12-15',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
            ]
            
        ]);

    return [
                'week_expense_sector_name_id' => 1,
                'week_expense_sub_sector_name_id' => 3,
                'week_expense_type_name_id' => 4,
                'name' => 'test1',
                'date' => '2019-12-15',
                'week' => '',
                'unit' => '',
                'poriman' => 100,
                'dor' => 5,
                'create_by_user_id' => 1,
                'update_by_user_id' => 1
    ];

});
