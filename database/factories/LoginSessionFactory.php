<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\LoginSession;
use Faker\Generator as Faker;

$factory->define(LoginSession::class, function (Faker $faker) {
    \App\LoginSession::truncate();
    return [
        'user_id' => 1,
        'session_code' => 'wte7qyWIGkPlr31oFQuwRYTFZbT8CRt0ohXn8mBuVAsMTDWgqEwH61JdgDiH',
        'is_expired' => 'no'
    ];
});
