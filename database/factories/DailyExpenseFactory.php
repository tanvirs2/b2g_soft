<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DailyExpense;
use Faker\Generator as Faker;

$factory->define(DailyExpense::class, function (Faker $faker) {
    return [
            "expense_sector_id" => $faker->numberBetween($min = 1, $max = 10),
            "expense_sub_sector_name" => $faker->word,
            "unit" => $faker->word,
            "per_unit_price" => $faker->numberBetween($min = 100, $max = 2000),
            "expense_date" => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'today'),
            "create_by_user_id" => "1",
            "update_by_user_id" => "1",
            "total_price" => $faker->numberBetween($min = 1000, $max = 20000),
            "payment_type_id" => "3"
    ];
});
