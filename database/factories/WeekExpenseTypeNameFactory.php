<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\WeekExpenseTypeName;
use Faker\Generator as Faker;

$factory->define(WeekExpenseTypeName::class, function (Faker $faker) {

    \App\WeekExpenseTypeName::truncate();
        \App\WeekExpenseTypeName::insert([
            [
                'id' => 1,
                'name' => 'খোড়াকী বাবদ'
            ],
            [
                'id' => 2,
                'name' => 'খাদলী'
            ],
            [
                'id' => 3,
                'name' => 'রিং ঝাড়ু'
            ],
            [
                'id' => 4,
                'name' => 'লাকড়ী'
            ],
            [
                'id' => 5,
                'name' => 'হ্যান্ডেল বাবদ'
            ],
            [
                'id' => 6,
                'name' => 'কাগজ'
            ],
            [
                'id' => 7,
                'name' => 'বিল দেয়া'
            ],
            [
                'id' => 8,
                'name' => 'বকেয়া'
            ],
            [
                'id' => 9,
                'name' => 'চা-চিনি'
            ],
            [
                'id' => 10,
                'name' => 'লাকড়ী'
            ],
            [
                'id' => 11,
                'name' => 'কনা ফেলানো'
            ],
            [
                'id' => 12,
                'name' => 'রাউন্ড বোনাস'
            ],
            [
                'id' => 13,
                'name' => 'যোগ গইড়া'
            ],
            [
                'id' => 14,
                'name' => 'বিবিধ'
            ],
            [
                'id' => 15,
                'name' => 'গেইট'
            ]
        ]);

    return [
        'id' => 16,
        'name' => 'আধলা ফেলানো'
    ];
});


