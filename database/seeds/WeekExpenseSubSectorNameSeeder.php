<?php

use Illuminate\Database\Seeder;

class WeekExpenseSubSectorNameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\WeekExpenseSubSectorName::truncate();

        /*\App\WeekExpenseSubSectorName::insert([
            [
                'name' => 'Mill1',
                'week_expense_sector_name_id' => 1,
            ],
            [
                'name' => 'Mill2',
                'week_expense_sector_name_id' => 1,
            ],
            [
                'name' => 'Mill3',
                'week_expense_sector_name_id' => 1,
            ],
            [
                'name' => 'Mill4',
                'week_expense_sector_name_id' => 1,
            ],
            [
                'name' => 'Mill5',
                'week_expense_sector_name_id' => 1,
            ],
            [
                'name' => 'MatiMixing1',
                'week_expense_sector_name_id' => 2,
            ],
            [
                'name' => 'MatiMixing2',
                'week_expense_sector_name_id' => 2,
            ],
            [
                'name' => 'MatiMixing3',
                'week_expense_sector_name_id' => 2,
            ],
            [
                'name' => 'MatiMixing4',
                'week_expense_sector_name_id' => 2,
            ]
        ]);*/
        /*if (\App\WeekExpenseSubSectorName::get()->count() == 0) {

        }*/
    }
}
