<?php

use Illuminate\Database\Seeder;

class DelData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\DailyIncome::truncate();
        \App\DailyExpense::truncate();
        \App\WeekExpenseSector::truncate();
        \App\Contract::truncate();
        \App\Khoraki::truncate();
        \App\PotGonona::truncate();
        \App\Remainder::truncate();
        \App\PersonalVault::truncate();
    }
}
