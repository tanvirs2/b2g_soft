<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //\App\DailyIncome::truncate();
        //\App\DailyExpense::truncate();
        //\App\WeekExpenseSector::truncate();
        //\App\Contract::truncate();
        //\App\Khoraki::truncate();
        //\App\PotGonona::truncate();
        //\App\Remainder::truncate();
        //\App\PersonalVault::truncate();
        //\App\LoginSession::truncate();

        $this->call(UserTableSeeder::class);
        $this->call(WeekExpenseSectorNameSeeder::class);
        factory(App\LoginSession::class)->create();
        factory(App\WeekExpenseTypeName::class)->create();
        factory(App\ExpenseSector::class)->create();
        factory(App\IncomeClass::class)->create();

        //\App\User::truncate();
        //\App\WeekExpenseSubSectorName::truncate();
        //$this->call(UserTableSeeder::class);


        //factory(App\WeekExpenseSector::class)->create();






        /*factory(App\User::class)->create();
        factory(App\LoginSession::class)->create();
        for ($i=1; $i <= 10; $i++) {
            factory(App\ExpenseSector::class)->create();
        }
        for ($i=1; $i <= 50; $i++) {
            factory(App\DailyExpense::class)->create();
        }*/
    }
}
