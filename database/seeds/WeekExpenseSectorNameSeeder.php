<?php

use Illuminate\Database\Seeder;

class WeekExpenseSectorNameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\WeekExpenseSectorName::truncate();
        \App\WeekExpenseSectorName::insert([
            [
                'id' => 1,
                'name' => 'মিল ১'
            ],
            [
                'id' => 2,
                'name' => 'মিল ২'
            ],
            [
                'id' => 3,
                'name' => 'মিল ৩'
            ],
            [
                'id' => 4,
                'name' => 'মিল ৪'
            ],
            [
                'id' => 5,
                'name' => 'মিল ৫'
            ],
            [
                'id' => 6,
                'name' => 'লোড'
            ],
            [
                'id' => 7,
                'name' => 'আনলোড'
            ],
            [
                'id' => 8,
                'name' => 'ফায়ারিং'
            ],
            [
                'id' => 9,
                'name' => 'মাটি মিক্সিং ১'
            ],
            [
                'id' => 10,
                'name' => 'মাটি মিক্সিং ২'
            ],
            [
                'id' => 11,
                'name' => 'মাটি মিক্সিং ৩'
            ],
            [
                'id' => 12,
                'name' => 'মাটি মিক্সিং ৪'
            ],
            [
                'id' => 13,
                'name' => 'বেতন'
            ],
            [
                'id' => 14,
                'name' => 'বিবিধ'
            ]
        ]);

        /*if (\App\WeekExpenseSectorName::get()->count() == 0) {

        }*/
    }
}
