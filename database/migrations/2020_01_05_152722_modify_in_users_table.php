<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('username')->nullable()->change();
            $table->string('contact_number')->unique()->change();
            $table->text('address')->nullable()->change();
            $table->integer('company_id')->nullable()->change();
            $table->string('email')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->nullable(false)->change();
            $table->string('username')->nullable(false)->change();
            $table->dropUnique('contact_number');
            $table->text('address')->nullable(false)->change();
            $table->integer('company_id')->nullable(false)->change();
            $table->string('email')->nullable(false)->change();
        });
    }
}
