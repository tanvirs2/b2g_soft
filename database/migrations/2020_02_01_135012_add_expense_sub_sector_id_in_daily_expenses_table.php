<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExpenseSubSectorIdInDailyExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('daily_expenses', function (Blueprint $table) {
            $table->integer('expense_sub_sector_id')->after('expense_sector_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daily_expenses', function (Blueprint $table) {
            $table->dropColumn('expense_sub_sector_id');
        });
    }
}
