<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDailyExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_expenses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('expense_sector_id');
            $table->string('expense_sub_sector_name')->nullable();
            $table->string('unit');
            $table->double('per_unit_price');
            $table->date('expense_date');
            //$table->date('create_date');
            $table->integer('create_by_user_id');
            $table->integer('update_by_user_id');
            //$table->date('update_date');
            $table->double('total_price');
            $table->integer('payment_type_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_expenses');
    }
}
