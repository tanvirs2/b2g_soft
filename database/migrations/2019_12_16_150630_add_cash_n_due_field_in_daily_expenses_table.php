<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCashNDueFieldInDailyExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('daily_expenses', function (Blueprint $table) {
            $table->float('cash')->after('per_unit_price')->nullable();
            $table->float('due')->after('per_unit_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daily_expenses', function (Blueprint $table) {
            $table->dropColumn('cash');
            $table->dropColumn('due');
        });
    }
}
