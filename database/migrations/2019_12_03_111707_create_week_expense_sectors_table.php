<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeekExpenseSectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('week_expense_sectors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('week_expense_sector_name_id')->nullable();
            $table->integer('week_expense_sub_sector_name_id')->nullable();
            $table->integer('week_expense_type_name_id')->nullable();
            $table->string('name')->nullable();
            $table->date('date')->nullable();
            $table->string('week')->nullable();
            $table->string('unit')->nullable();
            $table->string('poriman')->nullable();
            $table->string('dor')->nullable();
            $table->string('mot')->nullable();
            $table->integer('create_by_user_id')->nullable();
            $table->integer('update_by_user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('week_expense_sectors');
    }
}
