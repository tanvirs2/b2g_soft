<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePotGononasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pot_gononas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mill');
            $table->date('pot_date');
            $table->string('pot_no');
            $table->string('line');
            $table->string('porimap');
            $table->string('mot');
            $table->integer('create_by_user_id')->nullable();
            $table->integer('update_by_user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pot_gononas');
    }
}
