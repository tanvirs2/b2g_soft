import React from 'react';
import ReactDOM from 'react-dom';


class WeeklyReportComponent extends React.Component {
    render() {
        return <h1>Hello, {this.props.name}</h1>;
    }
}

export default WeeklyReportComponent;

if (document.getElementById('example')) {
    ReactDOM.render(<WeeklyReportComponent name={'tanvir'}/>, document.getElementById('example'));
}
