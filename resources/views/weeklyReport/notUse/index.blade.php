<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://unpkg.com/@babel/standalone/babel.min.js"></script>
    <script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>
    <title>Document</title>
</head>
<body>

<div id="app"></div>


<script src="{{ asset('js/app.js') }}"></script>

<script type="text/babel">

    class WeeklyReportComponent extends React.Component {
        constructor() {
            super();
            this.state = {
                weekExpenseName: []
            };
        }

        getWeekExpenseSectorName(){
            fetch('http://localhost/b2g_soft/public/api/week-expense-sector-name?token=wte7qyWIGkPlr31oFQuwRYTFZbT8CRt0ohXn8mBuVAsMTDWgqEwH61JdgDiH')
            .then(results => results.json())
            .then(results => this.setState({
                weekExpenseName: results.data
            }))
            ;
        }

        componentDidMount(){
            this.getWeekExpenseSectorName();
        }

        render() {
            return (
                <div>
                    <table border="1">
                        <tbody>
                        <tr>
                            <th>ExpenseSec</th>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Unit</th>
                            <th>Amount</th>
                            <th>Rate</th>
                            <th>Total</th>
                        </tr>

                        {this.state.weekExpenseName.map(function (item, index) {
                            return (
                                <tr>
                                    <td>{item.name}</td>
                                    <td>{item.date}</td>
                                    <td>{item.name}</td>
                                    <td>{item.unit}</td>
                                    <td>{item.poriman}</td>
                                    <td>{item.dor}</td>
                                    <td>{item.mot}</td>
                                </tr>
                            );
                        })}
                        </tbody>
                    </table>
                </div>
            );
        }
    }

    //export default WeeklyReportComponent;

    if (document.getElementById('app')) {
        ReactDOM.render(<WeeklyReportComponent/>, document.getElementById('app'));
    }

</script>
</body>
</html>
