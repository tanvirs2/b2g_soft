<?php

$dataArray = [
    'total' => []
];
$sl = 0;

?>


<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui, viewport-fit=cover">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Color theme for statusbar (Android only) -->
    <meta name="theme-color" content="#2196f3">
    <!-- Your app title -->
    <title>My App</title>
    <!-- Path to Framework7 Library Bundle CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/framework7/4.5.2/css/framework7.bundle.min.css">
</head>
<body>


<div id="app">
    <!-- this_weekly_week_expense_sector -->

    <!-- Your main view, should have "view-main" class -->
    <div class="view view-main view-init safe-areas">
        <div class="page" data-name="home">
            <!-- Scrollable page content-->
            <div class="page-content">
                @foreach($week_exp_sector_name as $exp_sector_name)

                    @php

                        $khorakiVal = 0;
                        $sectorTotal = 0;

                    @endphp

                    <div class="block-title"><h2 style="padding: 0; margin-bottom: 2px">({{ ++$sl }}) {{ $exp_sector_name->name }}</h2></div>
                    <div class="data-table card" style="background: #ecf0f1">
                        <table class="">
                            <thead>
                            <tr>
                                <th>সেক্টর</th>
                                <th>তারিখ</th>
                                <th>নাম</th>
                                <th>ইউনিট</th>
                                <th>পরিমাণ</th>
                                <th>দর</th>
                                <th>মোট</th>
                            </tr>
                            </thead>

                            @foreach($exp_sector_name->thisWeekly_weekExpenseSector as $this_weekly_week_expense_sector)

                                <tr class="my_row">
                                    <td class="id">
                                        {{ $this_weekly_week_expense_sector->weekExpenseTypeName->name ?? $exp_sector_name->name }}
                                    </td>
                                    <td>
                                        {{ $this_weekly_week_expense_sector->date }}
                                    </td>
                                    <td>
                                        {{ $this_weekly_week_expense_sector->name }}
                                    </td>
                                    <td>
                                        {{ $this_weekly_week_expense_sector->unit }}
                                    </td>
                                    <td class="poriman">
                                        {{ $this_weekly_week_expense_sector->poriman }}
                                    </td>
                                    <td class="dor">
                                        {{ $this_weekly_week_expense_sector->dor }}
                                    </td>
                                    <td class="total">
                                        {{ $dataArray['total'][] = $this_weekly_week_expense_sector->poriman * $this_weekly_week_expense_sector->dor }}
                                    </td>
                                </tr>
                            @endforeach

                            @php

                            $thisWeekly_weekExpenseSectors = $exp_sector_name->thisWeekly_weekExpenseSector;

                            $khorakiVal = $thisWeekly_weekExpenseSectors->where('week_expense_type_name_id', 0)->sum('poriman') - $thisWeekly_weekExpenseSectors->where('week_expense_type_name_id', 1)->sum('poriman');
                            $bill = $thisWeekly_weekExpenseSectors->where('week_expense_type_name_id', 7)->sum('poriman'); //বিল দেয়া
                            $sectorTotal = $exp_sector_name->thisWeekly_weekExpenseSector->whereNotIn('week_expense_type_name_id', [0,1,7,8])->sum('poriman') + $khorakiVal;
                            $sectorTotalWithDue = $sectorTotal - $bill;
                            @endphp

                            <tr style="background: #5ac8fa">
                                <td>বিল দেয়া</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ $bill }}</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="background: #5ac8fa">
                                <td>বকেয়া</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ $sectorTotalWithDue }}</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>পরিমাণ</th>
                                <th></th>
                                <th>মোট</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>{{--( {{ $exp_sector_name->name }} - খোড়াকী বাবদ )--}} {{ $dataArray['grandTotalPoriman'][] = $sectorTotal }} </th>
                                <th></th>
                                <th>{{ $dataArray['grandTotal'][] = array_sum($dataArray['total']) }} </th>
                            </tr>

                        </table>
                    </div>

                @endforeach

                <br>
                <hr>
                <br>

                <div class="data-table card" style="background: #f1c40f">
                        <table class="">
                            <tr>
                                <th>পরিমাণ</th>
                                <th>মোট</th>
                            </tr>
                            <tr>
                                <th> {{ array_sum($dataArray['grandTotalPoriman']) }} </th>
                                <th>{{ array_sum($dataArray['grandTotal']) }} </th>
                            </tr>

                        </table>
                    </div>
            </div>
        </div>
    </div>
</div>


<!-- Path to Framework7 Library Bundle JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/framework7/4.5.2/js/framework7.bundle.min.js"></script>
<!-- Path to your app js-->
<script type="text/javascript">
    var app = new Framework7({
        root: '#app', // App root element
    });

</script>
</body>
</html>
