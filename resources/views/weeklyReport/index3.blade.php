<?php

$dataArray = [
    'total' => [],
    'grandTotal' => [],
];
$sl = 0;

?>


<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui, viewport-fit=cover">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Color theme for statusbar (Android only) -->
    <meta name="theme-color" content="#2196f3">
    <!-- Your app title -->
    <title>My App</title>
    <!-- Path to Framework7 Library Bundle CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/framework7/4.5.2/css/framework7.bundle.min.css">

    <style>
        .loader {
            position: fixed;
            z-index: 99;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: white;
            display: flex;
            justify-content: center;
            align-items: center;
        }
    </style>

    <script>

    </script>

</head>
<body>

<div class="loader">
    <h3>Loading...</h3>
</div>

<div id="app">
    <div class="list no-hairlines-md" style="margin: 0">
        <ul>
            <li>
                <form>

                    @if (isset($_GET['db']))
                        <input type="hidden" name="db" readonly="readonly" value="{{ $_GET['db'] }}"/>
                    @endif

                    <div class="item-content item-input">
                        <div class="item-inner">
                            <div class="item-input-wrap">
                                <input type="text" name="date" placeholder="Weekly Report Filter" readonly="readonly" id="demo-calendar-default"/>
                            </div>
                        </div>
                    </div>
                    <div class="block" style="margin-top: 0; margin-bottom: 5px">
                        <p class="row">
                            <button class="col button button-raised">Filter</button>
                        </p>
                    </div>
                </form>
            </li>
        </ul>

    </div>
    <!-- this_weekly_week_expense_sector -->

    <!-- Your main view, should have "view-main" class -->
    <div class="view view-main view-init safe-areas">
        <div class="page" data-name="home">
            <!-- Scrollable page content-->
            <div class="page-content">
                @foreach($week_exp_sector_name as $exp_sector_name)

                    @php
                        $dataArray['total'] = [];
                        $sectionValue = 0;

                    @endphp

                    <div class="block-title"><h2 style="padding: 0; margin-bottom: 2px">({{ ++$sl }}) {{ $exp_sector_name->name }}</h2></div>
                    <div class="data-table card" style="background: #ecf0f1">
                        <table class="">
                            <thead>
                            <tr>
                                <th>খাত</th>
                                <th>তারিখ</th>
                                <th>নাম</th>
                                <th>ইউনিট</th>
                                <th>পরিমাণ</th>
                                <th>দর</th>
                                <th>মোট</th>
                            </tr>
                            </thead>

                            @foreach($exp_sector_name->thisWeekly_weekExpenseSector as $this_weekly_week_expense_sector)
                                @if($this_weekly_week_expense_sector->week_expense_type_name_id != 7 && $this_weekly_week_expense_sector->week_expense_type_name_id != 8)
                                <tr class="my_row">
                                    <td class="id">
                                        {{ $this_weekly_week_expense_sector->weekExpenseTypeName->name ?? $exp_sector_name->name }}
                                    </td>
                                    <td>
                                        @if($this_weekly_week_expense_sector->week_expense_type_name_id == 0 || $this_weekly_week_expense_sector->week_expense_type_name_id == 1)
                                            {{ $this_weekly_week_expense_sector->date }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ $this_weekly_week_expense_sector->name }}
                                    </td>
                                    <td>
                                        {{ $this_weekly_week_expense_sector->unit }}
                                    </td>
                                    <td class="poriman">
                                        {{ $this_weekly_week_expense_sector->poriman }}
                                    </td>
                                    <td class="dor">
                                        {{ $this_weekly_week_expense_sector->dor }}
                                    </td>
                                    <td class="total">

                                        {{ $this_weekly_week_expense_sector->unit * $this_weekly_week_expense_sector->poriman * $this_weekly_week_expense_sector->dor }}

                                        @php
                                            if($this_weekly_week_expense_sector->week_expense_type_name_id == 0){
                                                $sectionValue = $this_weekly_week_expense_sector->unit * $this_weekly_week_expense_sector->poriman * $this_weekly_week_expense_sector->dor;
                                            }
                                        @endphp

                                        @if($this_weekly_week_expense_sector->week_expense_type_name_id == 1)
                                            ({{ $dataArray['total'][] = $sectionValue - ($this_weekly_week_expense_sector->unit * $this_weekly_week_expense_sector->poriman * $this_weekly_week_expense_sector->dor) }})
                                        @endif

                                        @php
                                            if($this_weekly_week_expense_sector->week_expense_sector_name_id == 13 || $this_weekly_week_expense_sector->week_expense_sector_name_id == 14){
                                                $dataArray['total'][] = $this_weekly_week_expense_sector->unit * $this_weekly_week_expense_sector->poriman * $this_weekly_week_expense_sector->dor;
                                            }
                                            if($this_weekly_week_expense_sector->week_expense_type_name_id != 0 && $this_weekly_week_expense_sector->week_expense_type_name_id != 1){
                                                $dataArray['total'][] = $this_weekly_week_expense_sector->unit * $this_weekly_week_expense_sector->poriman * $this_weekly_week_expense_sector->dor;
                                            }
                                        @endphp
                                    </td>
                                </tr>
                                @endif
                            @endforeach

                            @php
                                $thisWeekly_weekExpenseSectors = $exp_sector_name->thisWeekly_weekExpenseSector;
                            @endphp
                            <tr style="background: #7bed9f">
                                <th>মোট</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>{{ $dataArray['grandTotal'][] = $total = array_sum($dataArray['total']) }} </th>
                            </tr>
                            @if($total && $exp_sector_name->id != 13 && $exp_sector_name->id != 14)
                            <tr style="background: #74b9ff">
                                <td>বিল দেয়া</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ $bill = $thisWeekly_weekExpenseSectors->where('week_expense_type_name_id', 7)->sum('dor') }}</td>
                            </tr>
                            <tr style="background: #ff7675">
                                <td>বকেয়া</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ ($total > 0) ? ($total - $bill): ($total + $bill)}}</td>
                            </tr>
                            @endif
                        </table>
                    </div>

                @endforeach

                <br>
                <hr>
                <br>

                <div class="data-table card" style="background: #f1c40f">
                        <table class="">
                            <tr>
                                <th> </th>
                                <th> সর্বমোট </th>
                                <th> </th>
                                <th> {{ array_sum($dataArray['grandTotal']) }} </th>
                                <th> </th>
                            </tr>

                        </table>
                    </div>
            </div>
        </div>
    </div>
</div>


<!-- Path to Framework7 Library Bundle JS-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/framework7/4.5.2/js/framework7.bundle.min.js"></script>
<!-- Path to your app js-->
<script type="text/javascript">

    var appCon = document.querySelector("#app");

    appCon.style.display = 'none';

    window.addEventListener('load', function () {

        const loader = document.querySelector(".loader");
        loader.style.display = 'none';
        appCon.style.display = 'block';

    });


    var app = new Framework7({
        root: '#app', // App root element
    });

    var calendarDefault = app.calendar.create({
        inputEl: '#demo-calendar-default',
    });

</script>
</body>
</html>
