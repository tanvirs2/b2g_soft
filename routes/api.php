<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/*Route::get('/users', function () {
    return \App\User::all();
});*/

//Dashboard
Route::get('dashboard-data', 'DashboardController@dashboardData');
//Route::get('total-daily-expense-sum', 'DashboardController@totalDailyExpenseSum'); // functionality exist
//Route::get('total-daily-income-sum', 'DashboardController@totalDailyIncomeSum'); // functionality exist


Route::resource('user', 'UserController');
Route::resource('daily-expense', 'DailyExpenseController');
Route::get('daily-expense-due-list', 'DailyExpenseController@dueList');
Route::get('daily-expense-due-into-null/{dailyExpense}', 'DailyExpenseController@dueIntoNull');

Route::resource('daily-income', 'DailyIncomeController');
Route::get('daily-income-due-list', 'DailyIncomeController@dueList');
Route::get('daily-income-due-into-null/{dailyIncome}', 'DailyIncomeController@dueIntoNull');

Route::resource('pot-gonona', 'PotGononaController');
Route::resource('expense-sector', 'ExpenseSectorController');
Route::resource('khoraki', 'KhorakiController');
Route::resource('remainder', 'RemainderController'); // reminder
Route::resource('support-center', 'SupportCenterController'); //suport
Route::resource('contract', 'ContractController');
Route::resource('personal-vault', 'PersonalVaultController');

/*Start V2 route*/
Route::resource('customer', 'CustomerController');
Route::resource('expense-sub-sector', 'ExpenseSubSectorController');
Route::resource('advance-module', 'AdvanceModuleController');
/*End V2 route*/

//weekly
Route::resource('week-expense-sector-name', 'WeekExpenseSectorNameController');
Route::resource('week-expense-sub-sector-name', 'WeekExpenseSubSectorNameController');
Route::resource('week-expense-type-name', 'WeekExpenseTypeNameController');
Route::resource('week-expense-sector', 'WeekExpenseSectorController');


//User login
Route::post('UserLogin', 'Auth\LoginController@userLogin')->name('UserLogin');


