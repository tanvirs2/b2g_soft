<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

/*Route::get('/weekly-report', function () {
    return view('weeklyReport.index');
});*/

//config(["database.default" => 'mysql2']);



Route::get('/weekly-report', 'WeekExpenseSectorController@weeklyReport');


Route::get('/ww', function () {

    //echo env('EXT_DB_DATABASE', 'nnnnnnnnnnnnnnnn');

    $sp = 1;

    foreach (\App\WeekExpenseSector::get() as $d) {
        //$d->id = $sp;
        $d->date = '2020-02-25';
        $d->save();

        $sp+=1;
    }
    //\App\WeekExpenseSector::all();
    //dd(\App\DailyExpense::find(5)->expense_sub_sector_name);
    //dd((new \App\DailyExpense())->totalDailyExpense());

    /*$monday = strtotime('last saturday', strtotime('tomorrow'));
    $sunday = strtotime('+6 days', $monday);
    echo "<P>". date('d-M-Y', $monday) . " to " . date('d-M-Y', $sunday) . "</P>";*/

    //return view('weeklyReport.index2');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
