<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeekExpenseSector extends Model
{
    public function weekExpenseSectorName()
    {
        return $this->belongsTo(WeekExpenseSectorName::class);
    }

    public function weekExpenseSubSectorName()
    {
        return $this->belongsTo(WeekExpenseSubSectorName::class);
    }

    public function weekExpenseTypeName()
    {
        return $this->belongsTo(WeekExpenseTypeName::class);
    }
    
}
