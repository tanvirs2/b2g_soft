<?php

namespace App;

use Illuminate\Database\Eloquent\Model, Illuminate\Support\Carbon;

class WeekExpenseSectorName extends Model
{
    public function weekExpenseSubSectorName()
    {
        return $this->hasMany(WeekExpenseSubSectorName::class);
    }

    public function weekExpenseSector()
    {
        return $this->hasMany(WeekExpenseSector::class);
    }

    public function thisWeekly_weekExpenseSector()
    {
        $now = Carbon::now();

        if (isset($_GET['date'])) {
            $date = $_GET['date'];
            $now = Carbon::parse($date);
        }

        //dd($now);

        $weekStartDate = $now->startOfWeek(Carbon::SATURDAY)->format('Y-m-d');
        $weekEndDate = $now->endOfWeek(Carbon::FRIDAY)->format('Y-m-d');
        //dd($weekEndDate);

        return $this->hasMany(WeekExpenseSector::class)->whereBetween('date', [$weekStartDate, $weekEndDate])->orderBy('id');
    }
}
