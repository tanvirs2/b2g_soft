<?php

namespace App\Http\Controllers;

use App\DailyExpense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DailyExpenseController extends Controller
{
    public $module = 'daily expense';

    public function dueIntoNull(Request $request, DailyExpense $dailyExpense)
    {
        try {
            $cashDueSum = $dailyExpense->cash + $dailyExpense->due;

            $dailyExpense->cash = $cashDueSum;
            $dailyExpense->due = 0;
            $dailyExpense->save();

            return $this->responseWithSuccess("$this->module", $dailyExpense);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    public function dueList(Request $request)
    {
        try {
            $query = DailyExpense::query()->whereNotBetween('due', [null, 0]);
            $data = $query->with('ExpenseSector')->get();
            return $this->responseWithSuccess("$this->module", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->tx); //in_single_field
        try {
            $query = DailyExpense::query();

            if ($request->filled('expense_sector_id')) {
                $query->where('expense_sector_id', $request->expense_sector_id);
            }

            if ($request->filled('expense_sub_sector_id')) {
                $query->where('expense_sub_sector_id', $request->expense_sub_sector_id);
            }

            if ($request->filled('date')) {
                $query->where('expense_date', $request->date);
            }

            if ($request->has('today')) {
                $query->where('expense_date', date('Y-m-d'));
            }

            if ($request->has('this_month')) {
                $query->whereYear('expense_date', date('Y'));
                $query->whereMonth('expense_date', date('m'));
            }

            if ($request->has('this_year')) {
                $query->whereYear('expense_date', date('Y'));
            }
            if ($request->has('in_single_field')) {
                $query = $query
                    ->select('expense_sector_id', DB::raw('SUM(unit) as unit, SUM(cash) as cash, SUM(due) as due, SUM(total_price) as total_price'))
                    ->groupBy('expense_sector_id')
                ;
            }
            $data = $query->with(['expenseSector', 'expenseSubSector'])->get();
            return $this->responseWithSuccess('found daily expense', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $dailyExpense = new DailyExpense();
            $dailyExpense->expense_sector_id = $request->expense_sector_id;

            //if ($request->filled('db') && $request->db == 'becwoqln_dev') {
                /*V2*/
                $dailyExpense->expense_sub_sector_id = $request->expense_sub_sector_id;
                /*V2*/
            //}


            $dailyExpense->expense_sub_sector_name = $request->expense_sub_sector_name;
            $dailyExpense->unit = $request->unit;
            $dailyExpense->per_unit_price = $request->per_unit_price;
            $dailyExpense->cash = $request->cash ?? 0;
            $dailyExpense->due = $request->due ?? 0;
            $dailyExpense->expense_date = $request->expense_date;
            $dailyExpense->create_by_user_id = $request->create_by_user_id;
            $dailyExpense->update_by_user_id = $request->update_by_user_id;
            $dailyExpense->total_price = $request->total_price;
            $dailyExpense->payment_type_id = $request->payment_type_id;
            $dailyExpense->save();

            return $this->responseWithSuccess('DailyExpense store');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DailyExpense  $dailyExpense
     * @return \Illuminate\Http\Response
     */
    public function show(DailyExpense $dailyExpense)
    {
        try {
            $data = $dailyExpense;

            return $this->responseWithSuccess('dailyExpense show', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DailyExpense  $dailyExpense
     * @return \Illuminate\Http\Response
     */
    public function edit(DailyExpense $dailyExpense)
    {
        try {
            $data = $dailyExpense;

            return $this->responseWithSuccess('edit dailyExpense', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DailyExpense  $dailyExpense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DailyExpense $dailyExpense)
    {
        try {
            //$dailyExpense = new DailyExpense();
            //$dailyExpense->expense_sector_id = $request->expense_sector_id;
            //$dailyExpense->expense_sub_sector_id = $request->expense_sub_sector_id;
            //$dailyExpense->expense_sub_sector_name = $request->expense_sub_sector_name;
            $dailyExpense->unit = $request->unit;
            $dailyExpense->per_unit_price = $request->per_unit_price;
            $dailyExpense->cash = $request->cash ?? 0;
            $dailyExpense->due = $request->due ?? 0;
            //$dailyExpense->expense_date = $request->expense_date;
            //$dailyExpense->create_by_user_id = $request->create_by_user_id;
            //$dailyExpense->update_by_user_id = $request->update_by_user_id;
            $dailyExpense->total_price = $request->total_price;
            //$dailyExpense->payment_type_id = $request->payment_type_id;
            $dailyExpense->save();

            return $this->responseWithSuccess('DailyExpense update');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DailyExpense  $dailyExpense
     * @return \Illuminate\Http\Response
     */
    public function destroy(DailyExpense $dailyExpense)
    {
        try {
            $data = $dailyExpense->delete();

            return $this->responseWithSuccess('delete dailyExpense', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
