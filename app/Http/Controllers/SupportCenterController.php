<?php

namespace App\Http\Controllers;

use App\SupportCenter;
use Illuminate\Http\Request;

class SupportCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = SupportCenter::query();

        try {
            $data = $query->get();

            return $this->responseWithSuccess("data list", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $supportCenter = new SupportCenter();
            $supportCenter->name = $request->remainder_name;
            $supportCenter->email = $request->email;
            $supportCenter->contract_number = $request->contract_number;

            $supportCenter->save();

            return $this->responseWithSuccess("data store");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SupportCenter  $supportCenter
     * @return \Illuminate\Http\Response
     */
    public function show(SupportCenter $supportCenter)
    {
        try {
            $data = $supportCenter;

            return $this->responseWithSuccess("data show", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SupportCenter  $supportCenter
     * @return \Illuminate\Http\Response
     */
    public function edit(SupportCenter $supportCenter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SupportCenter  $supportCenter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SupportCenter $supportCenter)
    {
        try {
            //$supportCenter = new SupportCenter();
            $supportCenter->name = $request->remainder_name;
            $supportCenter->email = $request->email;
            $supportCenter->contract_number = $request->contract_number;

            $supportCenter->save();

            return $this->responseWithSuccess("data update");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SupportCenter  $supportCenter
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupportCenter $supportCenter)
    {
        try {
            $data = $supportCenter->delete();

            return $this->responseWithSuccess("delete data");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
