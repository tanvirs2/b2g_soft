<?php

namespace App\Http\Controllers;

use App\Contract;
use Illuminate\Http\Request;

class ContractController extends Controller
{
    public $module = 'Contract';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Contract::query();

        if ($request->has('today')) {
            //$query->where('date', date('Y-m-d'));
        }
        try {
            //$data = DailyIncome::all();
            $data = $query->get();

            return $this->responseWithSuccess("$this->module", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $contract = new Contract();
            $contract->name = $request->name;
            $contract->email = $request->email;
            $contract->contract_number = $request->contract_number;
            $contract->create_by_user_id = $request->create_by_user_id;
            $contract->save();

            return $this->responseWithSuccess("$this->module store");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function show(Contract $contract)
    {
        try {
            $data = $contract;

            return $this->responseWithSuccess("$this->module show", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function edit(Contract $contract)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contract $contract)
    {
        try {
            $contract->name = $request->name;
            $contract->email = $request->email;
            $contract->contract_number = $request->contract_number;
            $contract->create_by_user_id = $request->create_by_user_id;
            $contract->save();

            return $this->responseWithSuccess("$this->module update");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contract $contract)
    {
        try {
            $data = $contract->delete();

            return $this->responseWithSuccess("delete $this->modul");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
