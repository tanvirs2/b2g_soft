<?php

namespace App\Http\Controllers;

use App\AdvanceModule;
use Illuminate\Http\Request;

class AdvanceModuleController extends Controller
{
    protected static function boot()
    {
        parent::boot();

        // Order by name ASC
        static::addGlobalScope('order', function ($builder) {
            $builder->orderBy('date', 'desc');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->tx);
        $query = AdvanceModule::query();

        if ($request->has('today')) {
            $query->where('date', date('Y-m-d'));
        }

        try {
            $data = $query->get();
            return $this->responseWithSuccess('found data', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $advanceModule = new AdvanceModule();
            $advanceModule->name = $request->name;
            $advanceModule->date = $request->date;
            $advanceModule->poriman = $request->poriman;
            $advanceModule->dor = $request->dor;
            $advanceModule->due = $request->due;
            $advanceModule->cash = $request->cash;
            $advanceModule->create_by_user_id = $request->create_by_user_id;
            $advanceModule->update_by_user_id = $request->update_by_user_id;
            $advanceModule->save();
            return $this->responseWithSuccess('data store');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdvanceModule  $advanceModule
     * @return \Illuminate\Http\Response
     */
    public function show(AdvanceModule $advanceModule)
    {
        try {
            $data = $advanceModule;

            return $this->responseWithSuccess('data show', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdvanceModule  $advanceModule
     * @return \Illuminate\Http\Response
     */
    public function edit(AdvanceModule $advanceModule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdvanceModule  $advanceModule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdvanceModule $advanceModule)
    {
        try {
            //$advanceModule = new AdvanceModule();
            $advanceModule->name = $request->name;
            $advanceModule->date = $request->date;
            $advanceModule->poriman = $request->poriman;
            $advanceModule->dor = $request->dor;
            $advanceModule->due = $request->due;
            $advanceModule->cash = $request->cash;
            $advanceModule->create_by_user_id = $request->create_by_user_id;
            $advanceModule->update_by_user_id = $request->update_by_user_id;
            $advanceModule->save();
            return $this->responseWithSuccess('data update');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdvanceModule  $advanceModule
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdvanceModule $advanceModule)
    {
        try {
            $data = $advanceModule->delete();

            return $this->responseWithSuccess('delete user', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
