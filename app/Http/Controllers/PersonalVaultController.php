<?php

namespace App\Http\Controllers;

use App\PersonalVault;
use Illuminate\Http\Request;

class PersonalVaultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = PersonalVault::query();

        try {
            $data = $query->get();

            return $this->responseWithSuccess("data list", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $personalVault = new PersonalVault();
            $personalVault->name = $request->name;
            $personalVault->msg = $request->msg;
            $personalVault->create_by_user_id = $request->create_by_user_id;
            $personalVault->update_by_user_id = $request->update_by_user_id;

            $personalVault->save();

            return $this->responseWithSuccess("data store");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PersonalVault  $personalVault
     * @return \Illuminate\Http\Response
     */
    public function show(PersonalVault $personalVault)
    {
        try {
            $data = $personalVault;

            return $this->responseWithSuccess("data show", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PersonalVault  $personalVault
     * @return \Illuminate\Http\Response
     */
    public function edit(PersonalVault $personalVault)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PersonalVault  $personalVault
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PersonalVault $personalVault)
    {
        try {
            //$personalVault = new PersonalVault();
            $personalVault->name = $request->name;
            $personalVault->msg = $request->msg;
            $personalVault->create_by_user_id = $request->create_by_user_id;
            $personalVault->update_by_user_id = $request->update_by_user_id;

            $personalVault->save();

            return $this->responseWithSuccess("data update");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PersonalVault  $personalVault
     * @return \Illuminate\Http\Response
     */
    public function destroy(PersonalVault $personalVault)
    {
        try {
            $data = $personalVault->delete();

            return $this->responseWithSuccess("delete data");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
