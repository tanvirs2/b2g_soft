<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = User::all();

            return $this->responseWithSuccess('All users found', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = new User();
            $user->name = $request->name;
            $user->username = $request->username;
            $user->password = bcrypt($request->password);
            $user->user_type_id = $request->user_type_id;
            $user->user_type_name = $request->user_type_name;
            $user->contact_number = $request->contact_number;
            $user->address = $request->address;
            $user->company_id = $request->company_id;
            $user->email = $request->email;
            $user->save();

            return $this->responseWithSuccess('user store');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = User::find($id);

            return $this->responseWithSuccess('user show', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $data = User::find($id);

            return $this->responseWithSuccess('edit user', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = User::find($id);
            $user->name = $request->name ?? $user->name;
            $user->username = $request->username ?? $user->username;
            $user->password = bcrypt($request->password) ?? $user->password;
            $user->user_type_id = $request->user_type_id ?? $user->user_type_id;
            $user->user_type_name = $request->user_type_name ?? $user->user_type_name;
            $user->contact_number = $request->contact_number ?? $user->contact_number;
            $user->address = $request->address ?? $user->address;
            $user->company_id = $request->company_id ?? $user->company_id;
            $user->email = $request->email ?? $user->email;
            $user->save();

            return $this->responseWithSuccess('update user', $user);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = User::destroy($id);

            return $this->responseWithSuccess('delete user', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
