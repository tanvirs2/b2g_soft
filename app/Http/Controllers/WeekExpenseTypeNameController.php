<?php

namespace App\Http\Controllers;

use App\WeekExpenseTypeName;
use Illuminate\Http\Request;

class WeekExpenseTypeNameController extends Controller
{
    public $module = 'WeekExpenseTypeName';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->tx);
        $query = WeekExpenseTypeName::query();

        if ($request->filled('week_expense_sector_id')) {
            //$query->where('expense_sector_id', $request->expense_sector_id);
        }

        if ($request->has('today')) {
            //$query->where('expense_date', date('Y-m-d'));
        }

        try {
            $data = $query->get();
            return $this->responseWithSuccess('found data', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $weekExpenseTypeName = new WeekExpenseTypeName();
            $weekExpenseTypeName->name = $request->name;
            $weekExpenseTypeName->others = $request->others;
            $weekExpenseTypeName->save();

            return $this->responseWithSuccess('data store');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WeekExpenseTypeName  $weekExpenseTypeName
     * @return \Illuminate\Http\Response
     */
    public function show(WeekExpenseTypeName $weekExpenseTypeName)
    {
        try {
            $data = $weekExpenseTypeName;

            return $this->responseWithSuccess('data show', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WeekExpenseTypeName  $weekExpenseTypeName
     * @return \Illuminate\Http\Response
     */
    public function edit(WeekExpenseTypeName $weekExpenseTypeName)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WeekExpenseTypeName  $weekExpenseTypeName
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WeekExpenseTypeName $weekExpenseTypeName)
    {
        try {
            //$weekExpenseTypeName = new WeekExpenseTypeName();
            $weekExpenseTypeName->name = $request->name;
            $weekExpenseTypeName->others = $request->others;
            $weekExpenseTypeName->save();

            return $this->responseWithSuccess('data update');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WeekExpenseTypeName  $weekExpenseTypeName
     * @return \Illuminate\Http\Response
     */
    public function destroy(WeekExpenseTypeName $weekExpenseTypeName)
    {
        try {
            $data = $weekExpenseTypeName->delete();

            return $this->responseWithSuccess('delete data', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
