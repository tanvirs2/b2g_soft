<?php

namespace App\Http\Controllers;

use App\ExpenseSubSector;
use Illuminate\Http\Request;

class ExpenseSubSectorController extends Controller
{
    public $module = 'Expense sub Sector';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $query = ExpenseSubSector::query();

            if ($request->has('today')) {
                //$query->where('date', date('Y-m-d'));
            }
            $data = $query->get();

            return $this->responseWithSuccess("$this->module", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $expenseSubSector = new ExpenseSubSector();
            $expenseSubSector->name = $request->name;
            $expenseSubSector->expense_sector_id = $request->expense_sector_id;
            $expenseSubSector->others = $request->others;
            $expenseSubSector->save();

            return $this->responseWithSuccess("$this->module store");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExpenseSubSector  $expenseSubSector
     * @return \Illuminate\Http\Response
     */
    public function show(ExpenseSubSector $expenseSubSector)
    {
        try {
            $data = $expenseSubSector;

            return $this->responseWithSuccess("$this->module show", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExpenseSubSector  $expenseSubSector
     * @return \Illuminate\Http\Response
     */
    public function edit(ExpenseSubSector $expenseSubSector)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExpenseSubSector  $expenseSubSector
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExpenseSubSector $expenseSubSector)
    {
        try {
            //$expenseSubSector = new ExpenseSubSector();
            $expenseSubSector->name = $request->name;
            $expenseSubSector->expense_sector_id = $request->expense_sector_id;
            $expenseSubSector->others = $request->others;
            $expenseSubSector->save();

            return $this->responseWithSuccess("$this->module update");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExpenseSubSector  $expenseSubSector
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExpenseSubSector $expenseSubSector)
    {
        try {
            $data = $expenseSubSector->delete();

            return $this->responseWithSuccess("delete $this->modul");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
