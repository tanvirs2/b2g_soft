<?php

namespace App\Http\Controllers;

use App\WeekExpenseSector;
use App\WeekExpenseSectorName;
use Illuminate\Http\Request, Illuminate\Support\Carbon;

class WeekExpenseSectorController extends Controller
{
    public function weeklyReport(Request $request){

        $weekExpenseSectorNameQuery = WeekExpenseSectorName::query();
        $data['week_exp_sector_name'] = $weekExpenseSectorNameQuery
        ->with(
            'thisWeekly_weekExpenseSector',
            'thisWeekly_weekExpenseSector.weekExpenseTypeName'
            )
        ->get()
        ;

        return view('weeklyReport.index3', $data);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->tx);
        $query = WeekExpenseSector::query();

        if ($request->filled('week_expense_sector_id')) {
            //$query->where('expense_sector_id', $request->expense_sector_id);
        }

        if ($request->has('today')) {
            //$query->where('expense_date', date('Y-m-d'));
        }

        try {
            $data = $query->get();
            return $this->responseWithSuccess('found data', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->week_expense_sector_list);
        try {
            foreach ($request->week_expense_sector_list as $week_expense_sector_single){
                $storeObj = (object)$week_expense_sector_single;
                if ($storeObj->week_expense_type_name_id != 8) {
                    $storeCapable = $storeObj->unit * $storeObj->poriman * $storeObj->dor;
                    if ($storeCapable) {
                        $weekExpenseSector = new WeekExpenseSector();
                        $weekExpenseSector->week_expense_sector_name_id = $storeObj->week_expense_sector_name_id;
                        //$weekExpenseSector->week_expense_sub_sector_name_id = $storeObj->week_expense_sub_sector_name_id;
                        $weekExpenseSector->week_expense_type_name_id = $storeObj->week_expense_type_name_id;
                        $weekExpenseSector->name = $storeObj->name;
                        $weekExpenseSector->date = $storeObj->date;
                        $weekExpenseSector->week = $storeObj->week;
                        $weekExpenseSector->unit = $storeObj->unit;
                        $weekExpenseSector->poriman = $storeObj->poriman;
                        $weekExpenseSector->dor = $storeObj->dor;
                        $weekExpenseSector->create_by_user_id = $storeObj->create_by_user_id;
                        $weekExpenseSector->update_by_user_id = $storeObj->update_by_user_id;
                        $weekExpenseSector->save();
                    }
                }
            }
            return $this->responseWithSuccess('data store');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WeekExpenseSector  $weekExpenseSector
     * @return \Illuminate\Http\Response
     */
    public function show(WeekExpenseSector $weekExpenseSector)
    {
        try {
            $data = $weekExpenseSector;

            return $this->responseWithSuccess('data show', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WeekExpenseSector  $weekExpenseSector
     * @return \Illuminate\Http\Response
     */
    public function edit(WeekExpenseSector $weekExpenseSector)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WeekExpenseSector  $weekExpenseSector
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WeekExpenseSector $weekExpenseSector)
    {
        try {
            //$weekExpenseSector = new WeekExpenseSector();
            $weekExpenseSector->week_expense_sector_name_id = $request->week_expense_sector_name_id;
            $weekExpenseSector->week_expense_sub_sector_name_id = $request->week_expense_sub_sector_name_id;
            $weekExpenseSector->week_expense_type_name_id = $request->week_expense_type_name_id;
            $weekExpenseSector->name = $request->name;
            $weekExpenseSector->date = $request->date;
            $weekExpenseSector->week = $request->week;
            $weekExpenseSector->unit = $request->unit;
            $weekExpenseSector->poriman = $request->poriman;
            $weekExpenseSector->dor = $request->dor;
            $weekExpenseSector->mot = $request->mot;
            $weekExpenseSector->create_by_user_id = $request->create_by_user_id;
            $weekExpenseSector->update_by_user_id = $request->update_by_user_id;
            $weekExpenseSector->save();

            return $this->responseWithSuccess('data update');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WeekExpenseSector  $weekExpenseSector
     * @return \Illuminate\Http\Response
     */
    public function destroy(WeekExpenseSector $weekExpenseSector)
    {
        try {
            $data = $weekExpenseSector->delete();

            return $this->responseWithSuccess('delete data', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
