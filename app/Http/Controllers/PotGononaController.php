<?php

namespace App\Http\Controllers;

use App\PotGonona;
use Illuminate\Http\Request;

class PotGononaController extends Controller
{
    public $module = 'pot gonona';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->tx);

        $query = PotGonona::query();

        if($request->filled('from') && $request->filled('to') && $request->filled('mill')) {
            $query = $query
            ->whereBetween('pot_date', [$request->get('from'), $request->get('to')] )
            ->where('mill', $request->get('mill'));
        }

        /*if ($request->filled('mill')) {
            $query = $query
                ->where('mill', $request->get('mill'));
        }*/

        try {
            $data = $query->get();
            return $this->responseWithSuccess("$this->module", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $potGonona = new PotGonona();
            $potGonona->mill = $request->mill;
            $potGonona->pot_date = $request->pot_date;
            $potGonona->pot_no = $request->pot_no;
            $potGonona->line = $request->line;
            $potGonona->porimap = $request->porimap;
            $potGonona->mot = $request->mot;
            $potGonona->save();

            return $this->responseWithSuccess("$this->module store");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PotGonona  $potGonona
     * @return \Illuminate\Http\Response
     */
    public function show(PotGonona $potGonona)
    {
        try {
            $data = $potGonona;

            return $this->responseWithSuccess('dailyExpense show', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PotGonona  $potGonona
     * @return \Illuminate\Http\Response
     */
    public function edit(PotGonona $potGonona)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PotGonona  $potGonona
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PotGonona $potGonona)
    {
        try {
            //$potGonona = new PotGonona();
            $potGonona->mill = $request->mill;
            $potGonona->pot_date = $request->pot_date;
            $potGonona->pot_no = $request->pot_no;
            $potGonona->line = $request->line;
            $potGonona->porimap = $request->porimap;
            $potGonona->mot = $request->mot;
            $potGonona->save();

            return $this->responseWithSuccess("$this->module update");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PotGonona  $potGonona
     * @return \Illuminate\Http\Response
     */
    public function destroy(PotGonona $potGonona)
    {
        try {
            $data = $potGonona->delete();

            return $this->responseWithSuccess("delete $this->modul");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
