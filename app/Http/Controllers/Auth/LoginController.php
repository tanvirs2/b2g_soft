<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Symfony\Component\HttpFoundation\Request, Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function userLogin(Request $request){

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        //dd($request->all());

        if(User::where('contact_number', $request->get('username'))->exists()){
            $user = User::where('contact_number', $request->get('username'))->first();
            $auth = Hash::check($request->get('password'), $user->password);
            if($user && $auth){

                $token = $user->rollApiKey(); //Model Function

                $data = [
                    'token' => $token,
                    'user'  => $user
                ];

                return $this->responseWithSuccess('Authorization Successful!', $data);
            }
        }
        return $this->responseWithError('Unauthorized, check your credentials.');
    }
}
