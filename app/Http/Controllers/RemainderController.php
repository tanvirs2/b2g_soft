<?php

namespace App\Http\Controllers;

use App\Remainder;
use Illuminate\Http\Request;

class RemainderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Remainder::query();

        try {
            $data = $query->get();

            return $this->responseWithSuccess("data list", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $remainder = new Remainder();
            $remainder->remainder_name = $request->remainder_name;
            $remainder->reason = $request->reason;
            $remainder->time = $request->time;
            $remainder->user_id = $request->user_id;
            $remainder->text = $request->text;

            $remainder->save();

            return $this->responseWithSuccess("data store");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Remainder  $remainder
     * @return \Illuminate\Http\Response
     */
    public function show(Remainder $remainder)
    {
        try {
            $data = $remainder;

            return $this->responseWithSuccess("data show", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Remainder  $remainder
     * @return \Illuminate\Http\Response
     */
    public function edit(Remainder $remainder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Remainder  $remainder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Remainder $remainder)
    {
        try {
            //$remainder = new Remainder();
            $remainder->remainder_name = $request->remainder_name;
            $remainder->reason = $request->reason;
            $remainder->time = $request->time;
            $remainder->user_id = $request->user_id;
            $remainder->text = $request->text;

            $remainder->save();

            return $this->responseWithSuccess("data update");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Remainder  $remainder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Remainder $remainder)
    {
        try {
            $data = $remainder->delete();

            return $this->responseWithSuccess("delete data");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
