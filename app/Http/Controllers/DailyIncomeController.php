<?php

namespace App\Http\Controllers;

use App\DailyIncome;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DailyIncomeController extends Controller
{
    public $module = 'daily income';

    public function dueIntoNull(Request $request, DailyIncome $dailyIncome)
    {
        try {
            $cashDueSum = $dailyIncome->cash + $dailyIncome->due;

            $dailyIncome->cash = $cashDueSum;
            $dailyIncome->due = 0;
            $dailyIncome->save();

            return $this->responseWithSuccess("$this->module", $dailyIncome);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    public function dueList(Request $request)
    {
        try {
            $query = DailyIncome::query()->whereNotBetween('due', [null, 0]);
            $data = $query->get();
            return $this->responseWithSuccess("$this->module", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $query = DailyIncome::query();

            if ($request->filled('date')) {
                $query->where('date', $request->date);
            }

            if ($request->filled('customer_id')) {
                $query->where('customer_id', $request->customer_id);
            }

            if ($request->has('today')) {
                $query->where('date', date('Y-m-d'));
            }

            if ($request->has('this_month')) {
                $query->whereYear('date', date('Y'));
                $query->whereMonth('date', date('m'));
            }

            if ($request->has('this_year')) {
                $query->whereYear('date', date('Y'));
            }

            if ($request->has('in_single_field')) {
                $query = $query
                    ->select('class_id', DB::raw('SUM(unit) as unit, SUM(cash) as cash, SUM(due) as due, SUM(total_price) as total_price'))
                    ->groupBy('class_id')
                ;
            }
            //$data = DailyIncome::all();
            $data = $query->get();

            return $this->responseWithSuccess("$this->module", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $dailyIncome = new DailyIncome();
            $dailyIncome->name = $request->name;

            //if ($request->filled('db') && $request->db == 'becwoqln_dev') {
                /*V2*/
                $dailyIncome->customer_id = $request->customer_id;
                /*V2*/
            //}

            $dailyIncome->date = $request->date;
            $dailyIncome->vehicle_no = $request->vehicle_no;
            $dailyIncome->class_id = $request->class_id;
            $dailyIncome->unit = $request->unit;
            $dailyIncome->per_unit_price = $request->per_unit_price;
            $dailyIncome->cash = $request->cash ?? 0;
            $dailyIncome->due = $request->due ?? 0;
            $dailyIncome->total_price = $request->total_price;
            $dailyIncome->payment_type_id = $request->payment_type_id;
            $dailyIncome->update_by_user_id = $request->update_by_user_id;
            $dailyIncome->create_by_user_id = $request->create_by_user_id;
            $dailyIncome->save();

            return $this->responseWithSuccess('Daily Income store', $dailyIncome);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DailyIncome  $dailyIncome
     * @return \Illuminate\Http\Response
     */
    public function show(DailyIncome $dailyIncome)
    {
        try {
            $data = $dailyIncome;

            return $this->responseWithSuccess('daily Income show', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DailyIncome  $dailyIncome
     * @return \Illuminate\Http\Response
     */
    public function edit(DailyIncome $dailyIncome)
    {
        try {
            $data = $dailyIncome;

            return $this->responseWithSuccess('edit daily Income', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DailyIncome  $dailyIncome
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DailyIncome $dailyIncome)
    {
        try {
            //$dailyIncome = new DailyIncome();
            //$dailyIncome->name = $request->name;
            //$dailyIncome->date = $request->date;
            //$dailyIncome->vehicle_no = $request->vehicle_no;
            //$dailyIncome->class_id = $request->class_id;
            //$dailyIncome->customer_id = $request->customer_id;
            $dailyIncome->unit = $request->unit;
            $dailyIncome->per_unit_price = $request->per_unit_price;
            $dailyIncome->cash = $request->cash ?? 0;
            $dailyIncome->due = $request->due ?? 0;
            $dailyIncome->total_price = $request->total_price;
            //$dailyIncome->payment_type_id = $request->payment_type_id;
            //$dailyIncome->update_by_user_id = $request->update_by_user_id;
            //$dailyIncome->create_by_user_id = $request->create_by_user_id;
            $dailyIncome->save();

            return $this->responseWithSuccess('Daily Income update', $dailyIncome);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DailyIncome  $dailyIncome
     * @return \Illuminate\Http\Response
     */
    public function destroy(DailyIncome $dailyIncome)
    {
        try {
            $data = $dailyIncome->delete();

            return $this->responseWithSuccess('delete daily Income', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
