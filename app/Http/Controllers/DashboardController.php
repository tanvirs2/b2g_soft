<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class DashboardController extends Controller
{
    public function totalDailyExpenseSum()
    {
        try {
            $data = (new \App\DailyExpense())->totalDailyExpenseSum();
            return $this->responseWithSuccess('daily expense', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    public function totalDailyIncomeSum()
    {
        try {
            $data['totalDailyIncomeSum'] = (new \App\DailyIncome())->totalDailyIncomeSum();
            return $this->responseWithSuccess("daily income", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    public function dashboardData()
    {
        try {
            $dailyIncome = new \App\DailyIncome();
            $dailyExpense = new \App\DailyExpense();

            // doinik beyer hisab = cash
            $data['totalDailyIncomeCashSum'] = round($dailyIncome->totalDailyIncomeCashSum(), 2);
            // doinik ayer hisab = cash
            $data['totalDailyExpenseCashSum'] = round($dailyExpense->totalDailyExpenseCashSum(), 2);

            // bokeya = all beyer due, all ayer due
            $data['incomeTotalDueSum'] = round($dailyIncome->totalYearlyDueSum(), 2);
            $data['expenseTotalDueSum'] = round($dailyExpense->totalYearlyDueSum(), 2);

            // mot balance =  all ayer cash - all beyer cash,
            $incomeTotalCashSum = $dailyIncome->totalYearlyCashSum();
            $expenseTotalCashSum = $dailyExpense->totalYearlyCashSum();

            $data['totalBalance'] = round($incomeTotalCashSum - $expenseTotalCashSum, 2);

            return $this->responseWithSuccess('dashboard data', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /*
     *
     * */
}
