<?php

namespace App\Http\Controllers;

use App\AppInfo;
use Illuminate\Http\Request;

class AppInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppInfo  $appInfo
     * @return \Illuminate\Http\Response
     */
    public function show(AppInfo $appInfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppInfo  $appInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(AppInfo $appInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppInfo  $appInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppInfo $appInfo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppInfo  $appInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppInfo $appInfo)
    {
        //
    }
}
