<?php

namespace App\Http\Controllers;

use App\Khoraki;
use Illuminate\Http\Request;

class KhorakiController extends Controller
{
    protected static function boot()
    {
        parent::boot();

        // Order by name ASC
        static::addGlobalScope('order', function ($builder) {
            $builder->orderBy('date', 'desc');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->tx);
        $query = Khoraki::query();

        if ($request->has('today')) {
            $query->where('date', date('Y-m-d'));
        }

        try {
            $data = $query->get();
            return $this->responseWithSuccess('found data', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->week_expense_sector_list);
        try {
            $khoraki = new Khoraki();
            $khoraki->name = $request->name;
            $khoraki->date = $request->date;
            $khoraki->unit = $request->unit;
            $khoraki->poriman = $request->poriman;
            $khoraki->dor = $request->dor;
            $khoraki->create_by_user_id = $request->create_by_user_id;
            $khoraki->update_by_user_id = $request->update_by_user_id;
            $khoraki->save();
            return $this->responseWithSuccess('data store');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Khoraki  $khoraki
     * @return \Illuminate\Http\Response
     */
    public function show(Khoraki $khoraki)
    {
        try {
            $data = $khoraki;

            return $this->responseWithSuccess('data show', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Khoraki  $khoraki
     * @return \Illuminate\Http\Response
     */
    public function edit(Khoraki $khoraki)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Khoraki  $khoraki
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Khoraki $khoraki)
    {
        //dd($request->week_expense_sector_list);
        try {
            //$khoraki = new Khoraki();
            $khoraki->name = $request->name;
            $khoraki->date = $request->date;
            $khoraki->unit = $request->unit;
            $khoraki->poriman = $request->poriman;
            $khoraki->dor = $request->dor;
            $khoraki->create_by_user_id = $request->create_by_user_id;
            $khoraki->update_by_user_id = $request->update_by_user_id;
            $khoraki->save();
            return $this->responseWithSuccess('data store');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Khoraki  $khoraki
     * @return \Illuminate\Http\Response
     */
    public function destroy(Khoraki $khoraki)
    {
        //
    }
}
