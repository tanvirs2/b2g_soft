<?php

namespace App\Http\Controllers;

use App\WeekExpenseSectorName;
use Illuminate\Http\Request;

class WeekExpenseSectorNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->tx);
        $query = WeekExpenseSectorName::query();

        if ($request->filled('week_expense_sector_id')) {
            //$query->where('expense_sector_id', $request->expense_sector_id);
        }

        if ($request->has('today')) {
            //$query->where('expense_date', date('Y-m-d'));
        }

        try {
            $data = $query->with('weekExpenseSubSectorName')->get();
            return $this->responseWithSuccess('found data', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $weekExpenseSectorName = new WeekExpenseSectorName();
            $weekExpenseSectorName->name = $request->name;
            $weekExpenseSectorName->others = $request->others;
            $weekExpenseSectorName->save();

            return $this->responseWithSuccess('data store');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WeekExpenseSectorName  $weekExpenseSectorName
     * @return \Illuminate\Http\Response
     */
    public function show(WeekExpenseSectorName $weekExpenseSectorName)
    {
        try {
            $data = $weekExpenseSectorName;

            return $this->responseWithSuccess('data show', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WeekExpenseSectorName  $weekExpenseSectorName
     * @return \Illuminate\Http\Response
     */
    public function edit(WeekExpenseSectorName $weekExpenseSectorName)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WeekExpenseSectorName  $weekExpenseSectorName
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WeekExpenseSectorName $weekExpenseSectorName)
    {

        try {
            //$weekExpenseSectorName = new WeekExpenseSectorName();
            $weekExpenseSectorName->name = $request->name;
            $weekExpenseSectorName->others = $request->others;
            $weekExpenseSectorName->save();

            return $this->responseWithSuccess('data update');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WeekExpenseSectorName  $weekExpenseSectorName
     * @return \Illuminate\Http\Response
     */
    public function destroy(WeekExpenseSectorName $weekExpenseSectorName)
    {
        try {
            $data = $weekExpenseSectorName->delete();

            return $this->responseWithSuccess('delete data', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
