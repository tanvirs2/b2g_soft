<?php

namespace App\Http\Controllers;

use App\ExpenseSector;
use Illuminate\Http\Request;

class ExpenseSectorController extends Controller
{
    public $module = 'Expense Sector';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = ExpenseSector::query();

        if ($request->has('today')) {
            //$query->where('date', date('Y-m-d'));
        }
        try {
            $data = $query->get();

            return $this->responseWithSuccess("$this->module", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $expenseSector = new ExpenseSector();
            $expenseSector->name = $request->name;
            $expenseSector->others = $request->others;
            $expenseSector->save();

            return $this->responseWithSuccess("$this->module store");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExpenseSector  $expenseSector
     * @return \Illuminate\Http\Response
     */
    public function show(ExpenseSector $expenseSector)
    {
        try {
            $data = $expenseSector;

            return $this->responseWithSuccess("$this->module show", $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExpenseSector  $expenseSector
     * @return \Illuminate\Http\Response
     */
    public function edit(ExpenseSector $expenseSector)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExpenseSector  $expenseSector
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExpenseSector $expenseSector)
    {
        try {
            $expenseSector->name = $request->name;
            $expenseSector->others = $request->others;
            $expenseSector->save();

            return $this->responseWithSuccess("$this->module update");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExpenseSector  $expenseSector
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExpenseSector $expenseSector)
    {
        try {
            $data = $expenseSector->delete();

            return $this->responseWithSuccess("delete $this->modul");
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
