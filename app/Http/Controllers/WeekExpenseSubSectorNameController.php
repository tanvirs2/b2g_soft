<?php

namespace App\Http\Controllers;

use App\WeekExpenseSubSectorName;
use Illuminate\Http\Request;

class WeekExpenseSubSectorNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->tx);
        $query = WeekExpenseSubSectorName::query();

        if ($request->filled('week_expense_sector_id')) {
            //$query->where('expense_sector_id', $request->expense_sector_id);
        }

        if ($request->has('today')) {
            //$query->where('expense_date', date('Y-m-d'));
        }

        try {
            $data = $query->with('weekExpenseSectorName')->get();
            return $this->responseWithSuccess('found data', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $weekExpenseSubSectorName = new WeekExpenseSubSectorName();
            $weekExpenseSubSectorName->name = $request->name;
            $weekExpenseSubSectorName->week_expense_sector_name_id = $request->week_expense_sector_name_id;
            $weekExpenseSubSectorName->others = $request->others;
            $weekExpenseSubSectorName->save();

            return $this->responseWithSuccess('data store');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WeekExpenseSubSectorName  $weekExpenseSubSectorName
     * @return \Illuminate\Http\Response
     */
    public function show(WeekExpenseSubSectorName $weekExpenseSubSectorName)
    {
        try {
            $data = $weekExpenseSubSectorName;

            return $this->responseWithSuccess('data show', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WeekExpenseSubSectorName  $weekExpenseSubSectorName
     * @return \Illuminate\Http\Response
     */
    public function edit(WeekExpenseSubSectorName $weekExpenseSubSectorName)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WeekExpenseSubSectorName  $weekExpenseSubSectorName
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WeekExpenseSubSectorName $weekExpenseSubSectorName)
    {
        try {
            //$weekExpenseSectorName = new WeekExpenseSectorName();
            $weekExpenseSubSectorName->name = $request->name;
            $weekExpenseSubSectorName->week_expense_sector_name_id = $request->week_expense_sector_name_id;
            $weekExpenseSubSectorName->others = $request->others;
            $weekExpenseSubSectorName->save();

            return $this->responseWithSuccess('data update');
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WeekExpenseSubSectorName  $weekExpenseSubSectorName
     * @return \Illuminate\Http\Response
     */
    public function destroy(WeekExpenseSubSectorName $weekExpenseSubSectorName)
    {
        try {
            $data = $weekExpenseSubSectorName->delete();

            return $this->responseWithSuccess('delete data', $data);
        } catch (\Exception $exception) {
            return $this->responseWithError($exception->getMessage());
        }
    }
}
