<?php

namespace App\Http\Controllers;

use App\IncomeClass;
use Illuminate\Http\Request;

class IncomeClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IncomeClass  $incomeClass
     * @return \Illuminate\Http\Response
     */
    public function show(IncomeClass $incomeClass)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IncomeClass  $incomeClass
     * @return \Illuminate\Http\Response
     */
    public function edit(IncomeClass $incomeClass)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IncomeClass  $incomeClass
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IncomeClass $incomeClass)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IncomeClass  $incomeClass
     * @return \Illuminate\Http\Response
     */
    public function destroy(IncomeClass $incomeClass)
    {
        //
    }
}
