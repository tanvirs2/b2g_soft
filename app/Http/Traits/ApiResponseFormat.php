<?php


namespace App\Http\Traits;


use Illuminate\Http\Request;

trait ApiResponseFormat
{
    public function __construct(Request $request)
    {
        if ($request->filled('db')) {
            $dbName = env('EXT_DB_PREFIX').'_'.$request->db;
            config(["database.connections.alt_mysql_db.database" => $dbName]);
            config(["database.default" => "alt_mysql_db"]);
        }
    }

    public function responseWithSuccess($message = '', $data = [], $code = 200)
    {
        return response()->json([
            'success' => true,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public function responseWithError($message = '', $data = [], $code = 400)
    {
        return response()->json([
            'success' => false,
            'message' => $message,
            'data' => $data,
        ], $code);
    }
}
