<?php

namespace App\Http\Middleware;

use Closure;

class DBChanger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->filled('db')) {
            $dbName = env('EXT_DB_PREFIX').'_'.$request->db;
            config(["database.connections.alt_mysql_db.database" => $dbName]);
            config(["database.default" => "alt_mysql_db"]);
        }
        return $next($request);
    }
}
