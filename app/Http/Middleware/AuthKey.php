<?php

namespace App\Http\Middleware;

use App\Http\Traits\ApiResponseFormat;
use App\LoginSession;
use Closure, Illuminate\Support\Facades\Route;

class AuthKey
{
    use ApiResponseFormat;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*$token = $request->header('APP_KEY');
        if ($token != 'b2g_app') {
            return response()->json(['massage' => 'App key not found'], 401);
        }*/

        //$token = $request->get('ttt');
        //dd($token);
        if (Route::is('UserLogin')) {
            return $next($request);
        }

        if(LoginSession::where('session_code', $request->get('token'))->exists()){
            $session_code = LoginSession::where('session_code', $request->get('token'))->first();
            if($session_code){
                return $next($request);
            }
        }

        return $this->responseWithError('token error');
    }
}
