<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseSubSector extends Model
{
    public function expenseSector()
    {
        return $this->belongsTo(ExpenseSector::class);
    }
}
