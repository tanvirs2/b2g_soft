<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DailyIncome extends Model
{
    protected static function boot()
    {
        parent::boot();

        // Order by name ASC
        static::addGlobalScope('order', function ($builder) {
            $builder->orderBy('date', 'desc');
        });
    }
    public function totalDailyIncomeSum($date = null)
    {
        if (!$date) {
            $date = date('Y-m-d');
        }
        return $this
            ->where('date', $date)
            ->get(DB::raw('sum(cash) + sum(due) AS cashDueTotal'))[0]
            ->cashDueTotal;
    }

    public function totalDailyIncomeCashSum($date = null)
    {
        if (!$date) {
            $date = date('Y-m-d');
        }

        $cash = $this->where('date', $date)
            ->sum('cash')
        ;

        return $cash;
    }

    public function totalYearlyDueSum($year = null)
    {
        /*if (!$year) {
            $year = date('Y');
        }

        $cash = $this->whereYear('expense_date', $year)
            ->sum('due')
        ;*/

        $due = $this
            ->sum('due')
        ;

        return $due;
    }
    public function totalYearlyCashSum($year = null)
    {
        $cash = $this
            ->sum('cash')
        ;

        return $cash;
    }
}
