<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseSector extends Model
{
    public function expenseSubSector()
    {
        return $this->hasMany(ExpenseSubSector::class);
    }
}
