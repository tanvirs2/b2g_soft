<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeekExpenseSubSectorName extends Model
{
    public function weekExpenseSectorName()
    {
        return $this->belongsTo(WeekExpenseSectorName::class);
    }

    public function weekExpenseSector()
    {
        return $this->hasMany(WeekExpenseSector::class);
    }
}
