<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class DBChangeAndSeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:seed:db {db_name} {--class=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $className = $this->option('class');
        $dbName = $this->argument('db_name');
        config(["database.connections.alt_mysql_db.database" => env('EXT_DB_PREFIX').'_'.$dbName]);
        config(["database.default" => "alt_mysql_db"]);
        if ($className) {
            Artisan::call('db:seed --class='.$className);
            $this->info($className.' file execute for '.$dbName);
        }
        Artisan::call('db:seed');
        $this->info(config("database.connections.alt_mysql_db.database").' database seeding complete');
    }
}
