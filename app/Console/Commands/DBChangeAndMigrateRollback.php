<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class DBChangeAndMigrateRollback extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:rollback:db {db_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rollback selected database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dbName = $this->argument('db_name');
        config(["database.connections.alt_mysql_db.database" => env('EXT_DB_PREFIX').'_'.$dbName]);
        Artisan::call('migrate:rollback --database=alt_mysql_db');
        $this->info(config("database.connections.alt_mysql_db.database").' Rollback Database');
    }
}
