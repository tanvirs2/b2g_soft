<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class DBChangeAndMigrate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:db {db_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate selected database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dbName = $this->argument('db_name');

        config(["database.connections.alt_mysql_db.database" => env('EXT_DB_PREFIX').'_'.$dbName]);
        //config(["database.default" => "alt_mysql_db"]);

        Artisan::call('migrate --database=alt_mysql_db'); //its work as default "database.default"
        $this->info(config("database.connections.alt_mysql_db.database").' Migrated Database');
    }
}
