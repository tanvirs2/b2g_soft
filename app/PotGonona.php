<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PotGonona extends Model
{
    protected static function boot()
    {
        parent::boot();

        // Order by name ASC
        static::addGlobalScope('order', function ($builder) {
            $builder->orderBy('pot_date', 'desc');
        });
    }
}
