<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyExpense extends Model
{
    protected static function boot()
    {
        parent::boot();

        // Order by name ASC
        static::addGlobalScope('order', function ($builder) {
            $builder->orderBy('expense_date', 'desc');
        });
    }
    public function expenseSector()
    {
        return $this->belongsTo(ExpenseSector::class);
    }

    public function expenseSubSector()
    {
        return $this->belongsTo(ExpenseSubSector::class);
    }

    public function totalDailyExpenseSum($date = null)
    {
        if (!$date) {
            $date = date('Y-m-d');
        }

        $cash = $this->where('expense_date', $date)
            ->sum('cash')
        ;
        $due = $this->where('expense_date', $date)
            ->sum('due')
        ;

        /*return $this
            ->where('expense_date', $date)
            ->get(DB::raw('sum(cash) + sum(due) AS cashDueTotal'))[0]
            ->cashDueTotal;*/

        return [
            'totalDailyExpenseSum' => $cash + $due, //cashDueTotal
            'totalDailyDueSum' => $due
        ];
    }

    public function totalDailyExpenseCashSum($date = null)
    {
        if (!$date) {
            $date = date('Y-m-d');
        }

        $cash = $this->where('expense_date', $date)
            ->sum('cash')
        ;

        return $cash;
    }
    public function totalYearlyDueSum($year = null)
    {
        /*if (!$year) {
            $year = date('Y');
        }

        $cash = $this->whereYear('expense_date', $year)
            ->sum('due')
        ;*/

        $due = $this
            ->sum('due')
        ;

        return $due;
    }

    public function totalYearlyCashSum($year = null)
    {
        $cash = $this
            ->sum('cash')
        ;

        return $cash;
    }
}
